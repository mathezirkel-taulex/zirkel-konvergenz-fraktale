% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\geometry{top=3cm}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\usepackage{wrapfig,lipsum}     % image to the side with text wrapped around

\renewcommand{\schuljahr}{2023/24}
\renewcommand{\klasse}{8}
\renewcommand{\titel}{Geometrische Reihe und Fraktalmaße}
\renewcommand{\untertitel}{}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
  \makeheader
  \vspace{-3em}


  \section{Die Geometrische Reihe}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{konvergenzen/geometric-easy.pdf}
    % \caption{Bekannte \emph{Beweise ohne Worte} für zwei geometrische Reihen.}
  \end{figure}


  Wir betrachten Reihen der folgenden Form:

  $$
    \sum \limits_{k = 0}^{\infty} \left( \dfrac{1}{2} \right)^k =
    1 + \dfrac{1}{2} + \dfrac{1}{4} + \dfrac{1}{8} + \dfrac{1}{16} + \dfrac{1}{32} + \dfrac{1}{64} + \dots
  $$

  $$
    \sum \limits_{k = 0}^{\infty} \left( \dfrac{1}{m} \right)^k =
    \dfrac{1}{m^0} + \dfrac{1}{m^1} + \dfrac{1}{m^2} + \dfrac{1}{m^3} + \dfrac{1}{m^4} + \dfrac{1}{m^5} + \dfrac{1}{m^6} + \dots \text{, mit } m \in \mathbb{N}^+
  $$

  {\boldmath
    $$
      \sum \limits_{k = 0}^{\infty} q^k =
      1 + q + q^2 + q^3 + q^4 + q^5 + q^6 + \dots \text{, mit } q \in (0, 1)
    $$
  }

  Eine Reihe dieser Form wird auch bezeichnet als \emph{geometrische Reihe}. Du kennst bereits das Ergebnis für spezielle Werte, wie für $q = \frac{1}{2} ,$ weil diese Ergebnisse sehr leicht zu veranschaulichen sind. Tatsächlich lässt es sich auch für jedes $q = \frac{1}{m} , \, m \in \{ 4, 5, 6, 7, \dots \} ,$ recht leicht veranschaulichen.


  \begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{konvergenzen/geometric-series-polygons-triangle.pdf}
    \caption{Alternativer Beweis ohne Worte für die geometrische Reihe mit $\frac{1}{4} .$}\label{fig:geopolstriangle}
  \end{figure}

  % \newpage
  \enlargethispage{4\baselineskip}


  \begin{aufgabe}
    Nutze die Visualisierung in Abbildungen~\ref{fig:geopolstriangle} und~\ref{fig:geopolsmore}, um die Grenzwerte für die geometrische Reihe für weitere Werte zu finden und zu beweisen.
  \end{aufgabe}





  \newpage
  \vspace*{-5em}



  \begin{aufgabe}\label{ex:easygeom}
    Berechne den Grenzwert der folgenden unendlichen Summen:
    \begin{enumerate}
      \item $
        \sum \limits_{k = 1}^{\infty} \left( \dfrac{1}{2} \right)^k =
        \dfrac{1}{2} + \dfrac{1}{4} + \dfrac{1}{8} + \dfrac{1}{16} + \dfrac{1}{32} + \dfrac{1}{64} + \dots =
      $ % 1
      \item $
        \sum \limits_{k = 1}^{\infty} \left( \dfrac{1}{4} \right)^k =
        \dfrac{1}{4} + \dfrac{1}{16} + \dfrac{1}{64} + \dfrac{1}{256} + \dfrac{1}{1024} + \dots =
      $ % 1/3
      \item $
        \sum \limits_{k = 0}^{\infty} \left( \dfrac{1}{3} \right)^k =
        1 + \dfrac{1}{3} + \dfrac{1}{9} + \dfrac{1}{27} + \dfrac{1}{81} + \dfrac{1}{243} + \dots =
      $ % 1,5 bzw 3/2
      \item $
        \sum \limits_{k = 1}^{\infty} \left( \dfrac{1}{7} \right)^k =
        \dfrac{1}{7} + \dfrac{1}{49} + \dfrac{1}{343} + \dfrac{1}{2401} + \dots =
      $ % 1/6
    \end{enumerate}

    \textbf{Bonus:} Betrachte ein einzelnes Bild aus Abbildung~\ref{fig:geopolsmore}. Um welchen Faktor ist die Höhe der zentralen, kleineren Form kleiner im Vergleich zur Höhe der äußeren Form?
  \end{aufgabe}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{konvergenzen/geometric-series-polygons-more.pdf}
    \caption{Weitere Beweise für die geometrische Reihe mit $\frac{1}{m}$ und $m \in \{ 5, 6, 7, \dots \} .$}\label{fig:geopolsmore}
  \end{figure}

  \begin{satz}[Grenzwert der Geometrischen Reihe]\label{satz:geom}
    Sei $q \in (-1, 1) ,$ also $-1 < q < 1 .$ Dann gilt:

    $$ \sum \limits_{k = 0}^{\infty} q^k = \dfrac{1}{1 - q} .$$
  \end{satz}


  \vspace{-1em}
  \begin{beweis}[Beweis von Satz~\ref{satz:geom}]
    \vspace{-2em}
    \begin{align*}
      (1 - q) \cdot \sum \limits_{k = 0}^{n} q^k
      &= (1 - q) \cdot (1 + q + q^2 + q^3 + \ldots + q^n) \\
      &= (1 + q + q^2 + q^3 + \ldots + q^n) - (q + q^2 + q^3 + \ldots + q^{n+1}) \\
      &= 1 - q^{n+1} \stackrel{n \to \infty}{\longrightarrow} 1
    \end{align*}
  \end{beweis}


  \enlargethispage{4\baselineskip}


  \begin{aufgabe}
    \vspace{-1em}
    \begin{enumerate}[a)]
      \item Warum ist es für den Beweis wichtig, dass $| q | < 1$ ?
      \item Überprüfe deine Ergebnisse aus Aufgabe~\ref{ex:easygeom} mit Satz~\ref{satz:geom}.
    \end{enumerate}
  \end{aufgabe}





  \newpage
  \vspace*{-5em}



  \section{Achilles und die Schildkröte}

  Der arrogante Achilles gibt einer scharfsinnigen Schildkröte bei einem Wettrennen einen Vorsprung.
  Als die überraschend schnelle Schildkröte schon fast die Mitte der Strecke erreicht hat, sprintet Achilles los.
  Er merkt, dass er doppelt so schnell wie die Schildkröte ist und sie deswegen noch vor dem Ziel einholen kann.
  Doch die Schildkröte ruft
  ``Gib auf, Achilles!
  Jedes Mal, wenn du meine alte Position erreicht hast, bin ich bereits ein ganzes Stück weiter gekommen.
  Du wirst nicht nur dieses Rennen verlieren.
  Du wirst mich nie einholen!''
  Achilles skizziert in seinen Gedanken die folgende Abbildung und rennt weiter...

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.55\linewidth]{konvergenzen/Zeno_Achilles_Paradox.png}
  \end{figure}

  \vspace{-1em}

  \begin{aufgabe}
    \vspace{-1em}
    \begin{enumerate}[a)]
      \item Die Schildkröte hat teils Recht.
        Wie kann es also sein, dass Achilles die Schildkröte einholt?
        % Achilles müsste zwar unendlich oft die alte Position der Schildkröte erreichen, und eine Sache unendlich oft machen kann niemand in einer Lebenszeit schaffen.
        % Aber die Sache, die Achilles unendlich oft machen muss, ist eine schnell genug kleiner werdende Strecke zu überwinden.
        % Dabei bleibt er nicht jedes Mal stehen und rennt aufs neue los, sodass auch die Zeit, die er dafür braucht, schnell genug kleiner wird.
        % Genauer entsprechen die Zeiten den unendlich vielen Summanden der geometrischen Reihe der Zahl 1/2 (insgesamt multipliziert mit der Zeit, die Achilles braucht, um die erste Schildkrötenposition zu erreichen).
      \item Als Achilles losrennt, hat die Schildkröte bereits $400$m zurückgelegt.
        Sie rennt halb so schnell, wie er.
        An welcher Stelle der Rennstrecke überholt er sie?
        % 400m * 2 = 800m
      \item Angenommen Achilles wäre nicht doppelt, sondern sechsmal so schnell wie die Schildkröte.
        Wann überholt er sie dann?
        % 400m * (6/5) = 480m
    \end{enumerate}
  \end{aufgabe}


  \enlargethispage{2.5\baselineskip}

  \section{Die Harmonische Reihe}

  \begin{equation*}
    \sum \limits_{i=1}^n \frac{1}{i} 
    = 1 + \frac{1}{2} + \frac{1}{3} + \frac{1}{4} + \frac{1}{5} + \frac{1}{6} + \frac{1}{7} + \frac{1}{8} + \ldots
  \end{equation*}
  
  \begin{satz}
    Die Harmonische Reihe wird unbeschränkt groß.
  \end{satz}

  \vspace{-1em}

  \begin{beweis}
    \vspace{-3em}
    \begin{equation*}
      \sum \limits_{i=1}^n \frac{1}{i}
      = 1 + \frac{1}{2} + \underbrace{\frac{1}{3} + \frac{1}{4}}_{\geq \frac{1}{4} + \frac{1}{4} = \frac{1}{2}} + \underbrace{\frac{1}{5} + \frac{1}{6} + \frac{1}{7} + \frac{1}{8}}_{\geq \frac{1}{8} + \frac{1}{8} + \frac{1}{8} + \frac{1}{8} = \frac{1}{2}} + \ldots
    \end{equation*}
  \end{beweis}






  \newpage
  \vspace*{-6em}



  \section{Fraktale in Konvergenz}

  \enlargethispage{4\baselineskip}

  Für die folgenden iterativen Fraktale ist bereits jeweils die Formel für den \textbf{Umfang} und für den \textbf{Flächeninhalt} angegeben für jede Iteration $n \in \mathbb{N} .$ Obwohl diese Maße für jede Iteration endlich groß sind, können sie unbeschränkt groß werden mit unbeschränkt steigender Iterationszahl. Sie können aber auch zu einem endlichen Wert konvergieren.

  \vspace*{-0.8em}

  \begin{aufgabe}
    \vspace*{-1em}
    \begin{itemize}
      \item Bestimme für jedes Fraktal, ob Umfang und Flächeninhalt unbeschränkt wachsen, oder ob sie gegen einen endlichen Grenzwert konvergieren.
      \vspace*{-0.6em}
      \item Berechne die endlichen Grenzwerte.
    \end{itemize}
  \end{aufgabe}

  \vspace*{-2em}

  \subsection{Sierpinski-Dreieck}
  \vspace*{-1em}

  \begin{figure}[h!]
      \centering
      \includegraphics[width=\linewidth]{fraktale/sierpinski-triangles-small.pdf}
  \end{figure}

  Umfang: $\left( \dfrac{3}{2} \right)^n$
  ~ \\

  Fläche: $\left( \dfrac{3}{4} \right)^n$
  ~\\
  \rule{\linewidth}{0.15mm}
  \vspace*{-2em}


  \subsection{Sierpinski-Teppich (vereinfacht)}
  \vspace*{-1em}
  
  \begin{figure}[h!]
      \centering
      \includegraphics[width=\linewidth]{fraktale/sierpinski-carpets-simple-small.pdf}
  \end{figure}

  Umfang: $\sum \limits_{k=0}^{n} \left( \dfrac{1}{3} \right)^k$
  ~ \\

  Fläche: $1 - \sum \limits_{k=1}^{n} \left( \dfrac{1}{9} \right)^k$
  ~\\
  \rule{\linewidth}{0.15mm}
  \vspace*{-2em}


  \subsection{Kochsche Schneeflocke}
  \vspace*{-1em}

  \begin{figure}[h!]
      \centering
      \includegraphics[width=\linewidth]{fraktale/koch-flakes-small.pdf}
  \end{figure}


  Umfang: $\left( \dfrac{4}{3} \right)^n$
  ~ \\

  Fläche: $1 + \dfrac{1}{3} \cdot \sum \limits_{k=0}^{n-1} \left( \dfrac{4}{9} \right)^k$
  ~\\
  \rule{\linewidth}{0.15mm}


\end{document}